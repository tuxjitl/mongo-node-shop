const mongodb = require("mongodb");

const MongoClient = mongodb.MongoClient;

let _db; // _ is gewoon om aan te geven dat dit enkel in deze file gebruikt wordt

const mongoConnect = (callback) => {
  MongoClient.connect(
    "mongodb+srv://jc:Seibukan1969@cluster0.iiduk.mongodb.net/shop?retryWrites=true&w=majority"
  )
    .then((client) => {
      console.log("We are connected");
      _db = client.db(); //zo ==> db is in connectionstring, anders dbnaam meegeven
      callback(client);
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw "No database found";
};

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;

/*
full driver example from atlas page
***********************************

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://<username>:<password>@cluster0.iiduk.mongodb.net/<dbname>?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.close();
});


*/
